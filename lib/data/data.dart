import 'package:questions_reponses/model/question.dart';

List<Question> getQuestion() {
  List<Question> listeQuestions = [];
  Question questionCourante = Question(question: '', isTrue: false);

  //1-----------------
  questionCourante.setQuestion("17 est un nombre premier");
  questionCourante.setReponse(true);
  questionCourante.setImage("https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/CuttySarkRomNum.jpg/400px-CuttySarkRomNum.jpg");
  listeQuestions.add(questionCourante);
  Question questionCourante = Question(question: '', isTrue: false);

  //2---------------
  questionCourante.setQuestion("La mission Apollo 11 c'est posés sur la lune en 1965");
  questionCourante.setReponse(false);
  questionCourante.setImage("https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Apollo_11_Lunar_Lander_-_5927_NASA.jpg/596px-Apollo_11_Lunar_Lander_-_5927_NASA.jpg");
  listeQuestions.add(questionCourante);
  Question questionCourante = Question(question: '', isTrue: false);

  //3--------------
  questionCourante.setQuestion("L'Assemblée Nationale Française compte 588 sièges");
  questionCourante.setReponse(false);
  questionCourante.setImage("https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Hemicycle_assemblee_nationale.JPG/640px-Hemicycle_assemblee_nationale.JPG");
  listeQuestions.add(questionCourante);
  Question questionCourante = Question(question: '', isTrue: false);

  return listeQuestions;

}