class Question{
  String question;
  bool reponse;
  String image = "";

  Question({required this.question, required this.isTrue});

  void setQuestion(String q){
    question = q;
  }
  String getQuestion(){
    return question;
  }

  void setReponse(bool r){
    reponse = r;
  }
  bool getReponse(){
    return reponse;
  }

  void setImage(String i){
    image = i;
  }
  String getImage(){
    return image;
  }

}