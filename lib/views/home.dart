import 'package:flutter/material.dart';
import 'package:questions_reponses/views/quizz.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

  class _HomeState extends State<Home> {
    @override
    Widget build(BuildContext context){
      return Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (builder) => Quizz()));
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 54),
                  decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(24)),
                  child: Text("Jouer",
                  style: TextStyle(color: Colors.grey, fontSize: 20),
                  ),
                 ),
                ),
               ],
              ),
             ),
            );
           }
      }