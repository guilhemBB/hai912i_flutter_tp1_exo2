import 'package:flutter/material.dart';
import 'package:questions_reponses/data/data.dart';
import 'package:questions_reponses/model/question.dart';
import 'package:questions_reponses/views/result.dart';


//inspiré de https://flutter-examples.com/flutter-horizontal-progress-bar-linearprogressindicator/


class Quizz extends StatefulWidget {
  @override
  _QuizzState createState() => _QuizzState();
}

class _QuizzState extends State<Quizz> with SingleTickerProviderStateMixin {
  List<Question> lQuestion = [];
  int index = 0;
  int points = 0;
  int correct = 0;
  int incorrect = 0;
  int nonRepondu = 0;

  late AnimationController controller;
  late Animation animation;
  double beginAnim = 0.0;
  double endAnim = 1.0;

  @override
  void initState() {
    super.initState();
    lQuestion = getQuestion();
    controller = AnimationController(duration: const Duration(seconds: 12), vsync: this);
    animation = Tween(begin: beginAnim, end: endAnim).animate(controller)
        ..addListener(() {
          setState(() {

          });
    });

    startProgress();

    animation.addStatusListener((AnimationStatus animationStatus) {
      if(animationStatus == AnimationStatus.completed) {
        if(index < lQuestion.length -1) {
          index++;
          resetProgress();
          startProgress();
        } else {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => Result(
                    score: points,
                    totalQuestion: lQuestion.length-1,
                    correct: correct,
                    incorrect: incorrect,
                    nonRepondu: nonRepondu,
                  )));
        }
      }
    });
  }

  startProgress(){
    controller.forward();
  }

  stopProgress(){
    controller.stop();
  }

  resetProgress(){
    controller.reset();
  }

  void nextQuestion() {
    if (index < lQuestion.length - 1){
      index++;
      resetProgress();
      startProgress();
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Result(
                score: points,
                totalQuestion: lQuestion.length -1,
                correct: correct,
                incorrect: incorrect,
                nonRepondu: nonRepondu,
              )));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 100),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "${index+1}/${lQuestion.length}",
                          style: TextStyle(
                          fontSize: 25, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Question",
                        style: TextStyle(
                          fontSize: 17, fontWeight: FontWeight.w300),
                        )
                    ],
                  ),
                  Spacer(),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                          "$points",
                        style: TextStyle(
                          fontSize: 25, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Points",
                        style: TextStyle(
                          fontSize: 19, fontWeight: FontWeight.w300),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Text(
                lQuestion[index].getQuestion() + "?",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 20,
                    fontWeight: FontWeight.w500),
                ),
                SizedBox(
                height: 20,
                ),
                Container(
                  child: LinearProgressIndicator(
                    value: animation.value,
                )),
                  NetworkImage(lQuestion[index].getImage()
                ),
                Spacer(),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                        onTap: () {
                          if(lQuestion[index].getReponse()) {
                            setState(() {
                              points += 20;
                              nextQuestion();
                              correct++;
                            });
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                            color: Colors.lightBlue,
                            borderRadius: BorderRadius.circular(24)),
                          child: Text(
                              "Vrai",
                              style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center,
                              ),
                            ),
                          )),
                          SizedBox(
                              width: 20,
                              ),
                          Expanded(
                              child: GestureDetector(
                          onTap: () {
                            if(!lQuestion[index].getReponse()){
                              setState(() {
                                points +=20;
                                nextQuestion();
                                correct++;
                              });
                            } else {
                              setState(() {
                                points -= 5;
                                nextQuestion();
                                incorrect++;
                              });
                            }
                          },
                          child: Container(
                              padding: EdgeInsets.symmetric(vertical: 12),
                              decoration: BoxDecoration(
                                  color: Colors.redAccent,
                                  borderRadius: BorderRadius.circular(24)),
                          child: Text(
                              "False",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w400),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )),
                        ],
                      ),
                      )
                    ],
                   ),
                  ),
                      );


}
    @override
    void dispose() {
      controller.dispose();
      super.dispose();
    }
    }