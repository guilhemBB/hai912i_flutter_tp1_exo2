import 'package:flutter/material.dart';
import 'package:questions_reponses/views/quizz.dart';

class Result extends StatefulWidget {
  int score;
  int totalQuestion;
  int correct;
  int incorrect;
  int nonRepondu;

  Result({required this.score, required this.totalQuestion,
    required this.correct, required this.incorrect,
    required this.nonRepondu});

  @override
  _ResultState createState() => _ResultState();
}

class _ResultState extends State<Result> {
  String greeting = "";


  @override
  void initState(){
    super.initState();
    var percentage = (widget.score / (widget.totalQuestion *20)) *100;
    if(percentage >= 90) {
      greeting = "Excellent";
    } else if (percentage >= 80 && percentage < 90) {
      greeting = "Très bien";
    } else if(percentage >= 70 && percentage < 80) {
      greeting = "Bien";
    } else greeting = "Vous ferez mieux la prochaine fois";
    }

    @override
    Widget buil(BuildContext context) {
      return Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                  "$greeting",
              style: TextStyle(
                color: Colors.black,
                fontSize: 24,
                fontWeight: FontWeight.w500,
              ),
              ),
              SizedBox(
                height: 14,
              ),
              Text("Résultat ${widget.score} / ${widget.totalQuestion*20}"),
              SizedBox(
                height: 8,
              ),
              Text("${widget.correct} réponses correctes, ${widget.incorrect} réponses incorrectes, ${widget.nonRepondu} hors délais"),
              SizedBox(
                height: 16,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (builder) => Quizz()));
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 54),
                  child: Text(
                    "Rejouer",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                    ),
                  decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(24),
                    color: Colors.lightBlue),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (builder) => Quizz()));
                  },
                child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: 12,
                  horizontal: 54,
                ),
                  child: Text("Retour à l'acceuil",
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 18,
                  )),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(24),
                    border: Border.all(color: Colors.blue, width: 2)),
                ),
              ),
            ],
          ),
        ),
      );
    }
    }